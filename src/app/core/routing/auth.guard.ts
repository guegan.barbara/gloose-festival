import { Injectable } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {

  constructor(public auth: AuthService) {}

  canActivate(): any {
    this.auth.isUserLogIn().subscribe((value: any) => { return value});
  }
}
