import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { LoginModalComponent } from 'src/app/shared/components/login-modal/login-modal.component';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  isLoggedIn : Observable<boolean>;
  isAdminLoggedIn : Observable<boolean>;
  isVolunteerLoggedIn : Observable<boolean>;

  constructor(private authService: AuthService, private dialog: MatDialog) {
    this.isLoggedIn = this.authService.isUserLogIn();
    this.isAdminLoggedIn = this.authService.isAdminLogIn();
    this.isVolunteerLoggedIn = this.authService.isVolunteerLogIn();
  }

  showFormLogin(): void {
    const loginModal = this.dialog.open(LoginModalComponent);
    loginModal.afterClosed().subscribe(result => { });
  }

  logout() {
    this.authService.logout();
  }
}
