import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './components/home/home.component';
import { PicturesComponent } from './components/pictures/pictures.component';
import { LatestNewsComponent } from './components/latest-news/latest-news.component';
import { NewsItemComponent } from './components/news-item/news-item.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [HomeComponent, PicturesComponent, LatestNewsComponent, NewsItemComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [HomeComponent]
})
export class HomeModule { }
