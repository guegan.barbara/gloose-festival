import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '../../services/auth.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent {
  userName = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);

  constructor(
    public dialogRef: MatDialogRef<LoginModalComponent>,
    private authService: AuthService) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.authService.authorizeLogin(this.userName.value, this.password.value);
    this.dialogRef.close();
  }
}
