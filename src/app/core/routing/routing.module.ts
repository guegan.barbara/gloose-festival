import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { HomeComponent } from 'src/app/modules/home/components/home/home.component';
import { AdminPageComponent } from 'src/app/modules/admin/components/admin-page/admin-page.component';
import { VolunteerPageComponent } from 'src/app/modules/volunteer/components/volunteer-page/volunteer-page.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'admin-planning', component: AdminPageComponent, canActivate: [AuthGuard] },
  { path: 'volunteer-planning', component: VolunteerPageComponent, canActivate: [AuthGuard] },
  { path: 'volunteer-form', component: VolunteerPageComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule { }
