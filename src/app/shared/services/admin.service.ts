import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  urlApi = 'https://test.com/';

  constructor(private httpClient: HttpClient) { }

  getActivities(): Observable<any> {
    const endPoint = this.urlApi + '/activities';
    return this.httpClient.post(endPoint, 'some datas');
  }
}
