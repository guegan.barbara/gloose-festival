import { Component } from '@angular/core';

@Component({
  selector: 'app-latest-news',
  templateUrl: './latest-news.component.html',
  styleUrls: ['./latest-news.component.scss']
})
export class LatestNewsComponent {
  newsList = [
    { title: 'Devenez bénévole !', date: '4 mars 2023', img: 'Benevoles-au-Publication-RS-Carree-60x60.png' },
    { title: 'Inscription aux animations', date: '7 juin 2022', img: 'Logo-Gloose.png' },
    { title: 'Chasse au trésor', date: '28 avril 2022', img: 'miniature-chasse-au-tresor-albi-jamaica-60x60.jpg' },
    { title: 'Dates 2022 et inscriptions', date: '15 avril 2022', img: 'GF8-Montage-benevoles-60x60.png' },
  ]

}
