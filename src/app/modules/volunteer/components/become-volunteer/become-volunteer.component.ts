import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-become-volunteer',
  templateUrl: './become-volunteer.component.html',
  styleUrls: ['./become-volunteer.component.scss']
})
export class BecomeVolunteerComponent implements OnInit {
  form!: FormGroup;
  activities = ['Accueil','Point infos','Buvette','Animation','Toutes'];
  dispo = ['Matin', 'Après-midi', 'Journée'];

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      phone: ['', { validators : [Validators.required], updateOn : 'blur'}],
      email: ['', { validators : [Validators.required /*Validators.pattern(RegExp.email)*/], updateOn : 'blur'}],
      activity: ['', [Validators.required]],
      availability: ['', [Validators.required]]
    });
  }
}
