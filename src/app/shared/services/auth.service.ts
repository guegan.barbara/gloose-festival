import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAdminConnected$ = new BehaviorSubject<boolean>(false);
  isVolunteerConnected$ = new BehaviorSubject<boolean>(false);

  private logins = [
    { userName: 'admin', password: 'admin' },
    { userName: 'volunteer', password: 'volunteer' }, // ne fonctionne pas
  ]

  isUserLogIn() : Observable<boolean> {
    return this.isAdminConnected$.asObservable() || this.isVolunteerConnected$.asObservable();
  }

  isAdminLogIn() : Observable<boolean> {
    return this.isAdminConnected$.asObservable();
  }

  isVolunteerLogIn() : Observable<boolean> {
    return this.isVolunteerConnected$.asObservable();
  }

  authorizeLogin(userName: any, password: any): void {
    this.logins.forEach(login => {
      if (userName === login.userName && password === login.password) {
        if (userName === 'admin') {
          this.isAdminConnected$.next(true);
        } else if (userName === 'volunteer') {
          this.isVolunteerConnected$.next(true);
        }
      }
    });
  }

  logout() {
    this.isAdminConnected$.next(false);
    this.isVolunteerConnected$.next(false);
  }
}
