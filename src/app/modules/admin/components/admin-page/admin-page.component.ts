import { Component, OnInit } from '@angular/core';
import { concat, filter, from, skip } from 'rxjs';
import { AdminService } from 'src/app/shared/services/admin.service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.sass']
})
export class AdminPageComponent implements OnInit {

  titles = [
    {text: 'Horaires', cols: 1, rows: 1, color: ''},
    {text: 'Accueil', cols: 1, rows: 1, color: ''},
    {text: 'Point infos', cols: 1, rows: 1, color: ''},
    {text: 'Buvette', cols: 1, rows: 1, color: ''},
    {text: 'Animation', cols: 1, rows: 1, color: ''},
  ];

  datas: any[] = [];
  hours: string[] = [];
  morningHours = ['0h','1h','3h','4h','5h', '6h', '7h', '8h', '9h', '10h', '11h'];
  eveningHours = ['12h','13h','14h','15h','16h','17h','18h','19h','20h','21h', '22h'];

  welcome = ['Julie','Julie','Benjamin','Benjamin','Anabelle','Anabelle','Sandrine','Sandrine','Jean','Jean','Julie','Julie','Paul','Paul'];
  infoPoint = ['Jean','Sandrine','Anabelle','Sandrine','Paul','Julie','Benjamin','Julie','Paul','Anabelle','Benjamin','Sandrine','Jean','Anabelle'];
  snackBar = ['Paul','Benjamin','Sandrine','Anabelle','Julie','Paul','Jean','Anabelle','Sandrine','Benjamin','Paul','Anabelle','Julie','Jean'];
  animation = ['Anabelle','Paul','test','Julie','Jean','Benjamin','Jean','Paul','test','Benjamin','Julie','Sandrine','Anabelle','Jean','test','Sandrine','Benjamin']
  newAnimation: string[] = [];

  colors = [
    {name:'Jean', color:'lightblue'},
    {name:'Julie', color:'lightgreen'},
    {name:'Paul', color:'lightpink'},
    {name:'Benjamin', color:'lightyellow'},
    {name:'Anabelle', color:'#DDBDF1'},
    {name:'Sandrine', color:'#E7F1BD'},
  ]

  constructor(private adminService: AdminService) { }

  ngOnInit() {
    this.getTitles();
  }

  getTitles() {
    // this.adminService.getActivities().subscribe({
    //   next: (response: any) => {
    //     this.titles = response;
    //     this.initPlanning();
    //   },
    //   error: (error) => {
    //     console.warn('getActivities error');
    //   }
    // });
    this.initPlanning();
  }

  initPlanning() {
    // Diverses actions sur les listes, qui n'ont pas vraiment de sens mais c'est pour utiliser les operators rxjs
    concat(from(this.morningHours).pipe(skip(8)), from(this.eveningHours))
      .subscribe(value => {
        this.hours.push(value)
      });

    from(this.animation).pipe(filter(value => value !== 'test')).subscribe(value => this.newAnimation.push(value));
    this.datas = this.hours.map((elem, index) => {
      return [
        {text: elem, cols:1, rows:1, color:''},
        {text: this.welcome[index], cols:1, rows:1, color: this.colors.find(color => color.name === this.welcome[index])?.color },
        {text: this.infoPoint[index], cols:1, rows:1, color: this.colors.find(color => color.name === this.infoPoint[index])?.color},
        {text: this.snackBar[index], cols:1, rows:1, color: this.colors.find(color => color.name === this.snackBar[index])?.color},
        {text: this.newAnimation[index], cols:1, rows:1, color: this.colors.find(color => color.name === this.newAnimation[index])?.color},
      ]
    }).flat()
  }

}
