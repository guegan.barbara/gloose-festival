import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VolunteerPageComponent } from './components/volunteer-page/volunteer-page.component';
import { BecomeVolunteerComponent } from './components/become-volunteer/become-volunteer.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [VolunteerPageComponent, BecomeVolunteerComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class VolunteerModule { }
